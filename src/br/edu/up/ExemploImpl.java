package br.edu.up;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ExemploImpl extends UnicastRemoteObject implements Exemplo {

	private static final long serialVersionUID = -1403317000770543357L;

	protected ExemploImpl() throws RemoteException {
		super();
	}

	@Override
	public String enviarMensagem(String mensagem) throws RemoteException {
		return "Mensagem enviada: " + mensagem;
	}

}
