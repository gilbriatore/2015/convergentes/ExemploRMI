package br.edu.up;

import java.rmi.Naming;

public class Cliente {
	
	public static void main(String[] args) throws Exception {
		
		if (args.length == 2){
			String url = new String("rmi//:" + args[0] + "/ExemploRMI");
			Exemplo exemploRMI = (Exemplo) Naming.lookup(url);
			String resposta = exemploRMI.enviarMensagem(args[1]);
			System.out.println("Reposta do servidor: " + resposta);
		} else {
			System.out.println("Modo de usar: Informe o nome ou IP do servidor e a mensagem!");
		}
	}
}
