package br.edu.up;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Exemplo extends Remote {
	String enviarMensagem(String mensagem) throws RemoteException;
}
