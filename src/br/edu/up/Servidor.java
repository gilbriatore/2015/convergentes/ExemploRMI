package br.edu.up;

import java.rmi.Naming;

public class Servidor {

	public static void main(String[] args) throws Exception {
		ExemploImpl exemploImpl = new ExemploImpl();
		Naming.bind("ExemploRMI", exemploImpl);
		System.out.println("Servidor RMI disponível!");
	}
}
